import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;

public class MainPerf {

	
	public static void main(String[] args) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		Chien pero = new Chien();
		pero.setNom("Rantamplan");
		int limit = 1000000;
		
		long avant = System.currentTimeMillis();		
		for(int i = 0; i < limit;i++){
			pero.getNom();
		}		
		long apres = System.currentTimeMillis();
		
		String prop = "nom";
		long avant2 = System.currentTimeMillis();	
		for(int i = 0; i < limit;i++){
			BeanUtils.getProperty(pero, prop);
		}		
		long apres2 = System.currentTimeMillis();
		
		System.out.println("Direct : "+(apres - avant)+"ms");
		System.out.println("Reflexivite : "+(apres2 - avant2)+"ms");
	}
}
