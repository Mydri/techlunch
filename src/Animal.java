
public abstract class Animal {
	private String nom;
	private Integer age;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Animal [nom=" + nom + ", age=" + age + "]";
	}

}
