import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.math.RandomUtils;

public class Main {

	public static void main(String[] args) throws Exception {
		Main main = new Main();
		Chien pero = main.newRandom(Chien.class);
		System.out.println(pero);
	}

	public void classData(Class<?> classe) {
		System.out.println(concatToString(classe.getFields())); // Attributs de la classe
		System.out.println(concatToString(classe.getMethods())); // m�thodes de la classe
		System.out.println(concatToString(classe.getAnnotations())); // annotations de la classe
	}

	public <T> T newRandom(Class<T> cible) throws Exception{
		T objet = cible.newInstance();
		for(Method methode : cible.getMethods()){
			if(methode.getName().startsWith("set") && methode.getParameters().length== 1){
				Parameter param = methode.getParameters()[0];
				if(param.getType() == String.class){
					methode.invoke(objet, UUID.randomUUID().toString());
				}else if(param.getType() == Integer.class){
					methode.invoke(objet, RandomUtils.nextInt());					
				}				
			}
		}
		return objet;
	}

	private String concatToString(Object[] array) {
		StringBuilder sb = new StringBuilder();
		for (Object object : array) {
			sb.append(object.toString() + "\t");
		}
		return sb.toString();
	}

	public Animal factory(Class<? extends Animal> classe) {
		if (classe == Chat.class) {
			return new Chat();
		} else if (classe == Chien.class) {
			return new Chien();
		} else if (classe == Lapin.class) {
			return new Lapin();
		} else if (classe == Cheval.class) {
			return new Cheval();
		}
		throw new UnsupportedOperationException();
	}

	public Animal genericFactory(Class<? extends Animal> classe) {
		try {
			return classe.newInstance();
		} catch (InstantiationException e) {
			throw new UnsupportedOperationException();
		} catch (IllegalAccessException e) {
			throw new UnsupportedOperationException();
		}
	}

	public void methodInfo(Object object, String methodName) {
		try {
			Method method = object.getClass().getMethod(methodName);
			System.out.println(method.getAnnotations()); // annotations
			System.out.println(method.getParameters()); // parametres
			System.out.println(method.getReturnType()); // type de retour
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}

	private void mettreToutaNull(Object object) {
		Method setterPero;
		try {
			setterPero = object.getClass().getMethod("setChien");
			setterPero.invoke(object, null);
			Method setterGato = object.getClass().getMethod("setChien");
			setterGato.invoke(object, null);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	private List<Method> getAllSetters() {
		return null;
	}

}
