@Deprecated
public class Zoo {

	private Chien pero;

	private Chat gato;
	
	public static final String NOM_ZOO="Templeton";

	public Chien getPero() {
		return pero;
	}

	public void setPero(Chien pero) {
		this.pero = pero;
	}

	public Chat getGato() {
		return gato;
	}

	public void setGato(Chat gato) {
		this.gato = gato;
	}

}
