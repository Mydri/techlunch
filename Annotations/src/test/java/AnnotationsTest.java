import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.opensource.lsevere.techlunch.annotations.runner.MyContext;
import fr.opensource.lsevere.techlunch.annotations.runner.Traitement;

public class AnnotationsTest {
	ApplicationContext ctx;

	@Autowired
	private Traitement traitement;

	@Before
	public void setUp() {
		ctx = new AnnotationConfigApplicationContext(MyContext.class);
		traitement = ctx.getBean("traitement", Traitement.class);
	}

	@Test
	public void simpleAnnotationsHibernate() {
		traitement.simpleAnnotationsHibernate();
	}

	@Test
	public void simpleAnnotationsHibernateReccursive() {
		traitement.simpleAnnotationsHibernateReccursive();
	}

	@Test
	public void simpleAnnotationsSpring() {
		traitement.simpleAnnotationsSpring();
	}

	@Test
	public void customAnnotationsSpring() {
		traitement.simpleAnnotationsSpring();
	}

}
