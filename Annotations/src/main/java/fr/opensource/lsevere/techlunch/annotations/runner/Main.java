package fr.opensource.lsevere.techlunch.annotations.runner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

	public static void main(String[] args) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(MyContext.class);
		((Traitement)ctx.getBean("traitement")).start();
	}

}
