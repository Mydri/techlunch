package fr.opensource.lsevere.techlunch.annotations.model;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import fr.opensource.lsevere.techlunch.annotations.validation.IDoitValider;

public class BasicAnotationsSubModel {

	@Email
	//@NotEmpty(groups = IDoitValider.class)
	private String string2;

	public String getString2() {
		return string2;
	}

	public void setString2(String string2) {
		this.string2 = string2;
	}

}
