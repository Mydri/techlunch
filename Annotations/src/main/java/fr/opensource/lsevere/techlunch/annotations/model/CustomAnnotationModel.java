package fr.opensource.lsevere.techlunch.annotations.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class CustomAnnotationModel {

	@NotEmpty
	private String string1;

	@NotNull
	@Valid
	private BasicAnotationsSubModel object1;

	public String getString1() {
		return string1;
	}

	public void setString1(String string1) {
		this.string1 = string1;
	}

	public BasicAnotationsSubModel getObject1() {
		return object1;
	}

	public void setObject1(BasicAnotationsSubModel object1) {
		this.object1 = object1;
	}

}
