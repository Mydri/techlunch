package fr.opensource.lsevere.techlunch.annotations.runner;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import fr.opensource.lsevere.techlunch.annotations.model.BasicAnotationsModel;
import fr.opensource.lsevere.techlunch.annotations.model.BasicAnotationsSubModel;
import fr.opensource.lsevere.techlunch.annotations.validation.IDoitValider;

@Component
@Qualifier("traitement")
public class Traitement {

	@Autowired
	private Validator hibernateValidator;

	@Autowired
	private Validator springValidator;

	public void start() {
		System.err.println("D�but");
		if (hibernateValidator == springValidator)
			throw new RuntimeException("Erreur dans les injections");

		simpleAnnotationsHibernate();
		simpleAnnotationsSpring();
		simpleAnnotationsHibernateReccursive();
	}

	public void simpleAnnotationsHibernate() {
		System.err.println("\nsimpleAnnotationsHibernate");
		BasicAnotationsModel model = new BasicAnotationsModel();
		Set<ConstraintViolation<BasicAnotationsModel>> erreurs = hibernateValidator.validate(model);
		printResult(erreurs);
	}

	public void simpleAnnotationsHibernateReccursive() {
		System.err.println("\nsimpleAnnotationsHibernateReccursive");
		BasicAnotationsModel model = new BasicAnotationsModel();
		model.setObject1(new BasicAnotationsSubModel());
		model.setString1("-");
		Set<ConstraintViolation<BasicAnotationsModel>> erreurs = hibernateValidator.validate(model, IDoitValider.class);
		printResult(erreurs);
	}

	public void simpleAnnotationsSpring() {
		System.err.println("\nsimpleAnnotationsSpring");
		BasicAnotationsModel model = new BasicAnotationsModel();
		Set<ConstraintViolation<BasicAnotationsModel>> erreurs = springValidator.validate(model);
		printResult(erreurs);
	}

	private void printResult(Set<ConstraintViolation<BasicAnotationsModel>> erreurs) {
		System.err.println("R�sultat : ");
		for (ConstraintViolation<BasicAnotationsModel> violation : erreurs) {
			System.err.println(violation.getMessage() + " : " + violation.getPropertyPath());
		}
	}

}
