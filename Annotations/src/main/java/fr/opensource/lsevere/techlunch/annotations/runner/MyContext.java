package fr.opensource.lsevere.techlunch.annotations.runner;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
@ComponentScan("fr.opensource.lsevere.techlunch.annotations")
public class MyContext {

	@Bean
	@Qualifier("hibernateValidator")
	public Validator hibernateValidator() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		return factory.getValidator();
	}

	@Bean
	@Qualifier("springValidator")
	public org.springframework.validation.Validator springValidator() {
		LocalValidatorFactoryBean factory = new LocalValidatorFactoryBean();
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("messages");
		factory.setValidationMessageSource(messageSource);
		return factory;
	}

	@Bean
	public ResourceBundleMessageSource messages() {
		ResourceBundleMessageSource result = new ResourceBundleMessageSource();
		result.setBasename("messages");
		return result;
	}
}
