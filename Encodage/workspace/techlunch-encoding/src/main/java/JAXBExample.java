import java.io.File;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class JAXBExample {

	public static void main(String[] args) {
		//unmarshall();
		marshall();
	}
	
	public static void unmarshall() {

		try {

			InputStream is = JAXBExample.class.getClassLoader().getResourceAsStream("xml.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(Customer.class);

			javax.xml.bind.Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Customer customer = (Customer) jaxbUnmarshaller.unmarshal(is);
			System.out.println(customer);

		} catch (JAXBException e) {
			e.printStackTrace();
		}

	}

	public static void marshall() {

		Customer customer = new Customer();
		customer.setId(100);
		customer.setName("���");
		customer.setAge(29);

		try {

			File file = new File("target/out.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(Customer.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, StandardCharsets.ISO_8859_1.name());

			jaxbMarshaller.marshal(customer, file);
			jaxbMarshaller.marshal(customer, System.out);

		} catch (JAXBException e) {
			e.printStackTrace();
		}

	}
}